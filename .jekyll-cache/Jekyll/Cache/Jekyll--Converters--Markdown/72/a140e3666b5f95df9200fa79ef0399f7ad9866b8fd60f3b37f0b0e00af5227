I"�<h4 id="core-research-themes">Core research themes</h4>

<p><u>The consequences of soil microbes for plant diversity</u></p>

<p>Microorganisms are ubiquitous and can have profound effects on plant communities. Indeed, mutualistic interactions between plants and soil fungi are thought to have been critical in allowing plants to colonize land. A <a href="https://en.wikipedia.org/wiki/Plant%E2%80%93soil_feedback">pioneering theoretical framework</a> developed two decades ago has motivated a great deal of empirical research that has shown that dynamic feedbacks between plants and soil microbes <a href="http://science.sciencemag.org/content/355/6321/134">have the potential to promote species diversity</a> in plant community. As part of my dissertation, I have extended ecological theory to explore how soil microbes may promote (or hinder) plant diversity via mechanisms that have not been clear in the literature so far. My work has highlighted the importance of quantifying the degree to which soil microbes mediate average fitness differences between plants that favor species exclusion – an effect that has received little empirical attention.</p>

<p><img src="../assets/greenhouse.jpg" alt="" /></p>
<div align="center">
<small><i>
Seedlings of annual plants for a plant-microbe interaction experiment
</i></small>
</div>

<p><u>Using plant functional traits to understand species interactions in heterogeneous environments</u></p>

<p>A central aim in ecology is to explain variation in species composition across space and to predict species demography over environmental gradients. Although variation in functional traits means along gradients is a well-documented pattern, we lack a clear understanding of how <a href="https://pdfs.semanticscholar.org/664b/fe6984faf7a975896626b4875fa82faa1b67.pdf?_ga=2.139439987.1524695756.1544747545-512070160.1524722723">plant functional traits</a> can help predict species’ abundance and demography in heterogeneous landscapes. A core part of my dissertation research explores how variation in functional traits - both within and across species - helps us understand species’ demographic response to environmental variation. I conduct this research in the beautiful and diverse annual plant community in the University of California’s <a href="http://sedgwick.nrs.ucsb.edu/">Sedgwick Reserve</a>.</p>

<p><img src="../assets/sedgwick.jpg" alt="" /></p>
<div align="center">
<small><i>
Spring at Sedgwick Reserve
</i></small>
</div>

<p><br /></p>

<hr />

<h4 id="other-research-and-teaching-projects">Other research and teaching projects</h4>
<p><u>Using environmental DNA for biodiversity research</u></p>

<p>During my PhD at UCLA, I’ve also had the pleasure of collaborating on various research and teaching projects. I worked with <a href="https://www.ioes.ucla.edu/person/emily-curd/">Dr. Emily Curd</a>, <a href="http://www.zackgold.org/">Zack Gold</a>, and other members of the <a href="http://www.ucedna.com/">California environmental DNA program</a> to develop an <a href="https://www.biorxiv.org/content/early/2018/12/07/488627">bioinformatics pipeline</a> to help automate the process of assigning taxonomy to sequences in eDNA projects. I also led the development of a <a href="https://f1000research.com/articles/7-1734/v1"><code class="highlighter-rouge">R</code> package/<code class="highlighter-rouge">shiny</code> app</a> to help visualize results from environmental DNA sequencing studies which has been used by various undergraduate microbiology and eDNA classes at UCLA.</p>

<p><u>How do consumers shape plant species coexistence?</u><br />
The dynamics of natural plant communities are governed not only by interactions that directly occur between plant species, but also by the actions of consumers. As part of my work at Sedgwick Reserve, I have collaborated with <a href="https://wpetry.github.io/">Dr. Will Petry</a> to ask how granivorous ants that harvest the seeds of annual plant species promote or erode diversity of the plant community.</p>

:ET