I"�<p>Hi. Thanks for visiting my website.</p>

<p>I am a PhD student in <a href="https://sites.lifesci.ucla.edu/eeb-kraft/">Dr. Nathan Kraft</a>’s plant community ecology research group at the <a href="https://www.ucla.edu">University of California, Los Angeles</a>. I like to spend my time thinking about the origins, maintenance, structure, and consequences of diversity.</p>

<p><img src="assets/white-mtns.jpg" alt="" /></p>
<div align="center">
<small><i>
Sunrise at the White Mountains
</i></small>
</div>

<p><br /></p>

<p>For my dissertation research, I work with the annual plant community in <a href="http://sedgwick.nrs.ucsb.edu/">Sedgwick Reserve</a> to ask how plant functional traits – traits that summarize plant ecological strategies – help explain how species interact across environmental gradients. I am also keenly interested in learning about the role of dynamic feedbacks between plants and soil microbial communities in maintaining plant diversity, and am pursuing theoretical and experimental research on this question. For more information on my dissertation research and the side-projects that keep me occupied, visit the <a href="projects/">projects</a> page, take a look at my <a href="assets/GSK_CV.pdf">CV</a>, and <a href="mailto::gaurav.kandlikar@gmail.com">email me</a> with any questions.</p>

<p>During my time at UCLA, I am also trying to make an effort to build my sense of environmental and educational ethics. Through <a href="https://www.edx.org/course/an-introduction-to-evidence-based-undergraduate-stem-teaching">workshops</a>, talking to students, and reading <a href="https://successfulstemeducation.org/about/nrc-report">reports</a>, I have learned a great deal about the barriers that minority students face in science classrooms. I am committed to doing my part in helping minimize these barriers, but I don’t claim to have any answers. I am always eager to have conversations about this topic, and would be happy to get any reading recommendations.</p>
:ET