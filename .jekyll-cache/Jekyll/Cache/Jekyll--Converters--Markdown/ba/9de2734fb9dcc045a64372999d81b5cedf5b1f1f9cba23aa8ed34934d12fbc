I"M <p>I had the incredible opportunity to explore <a href="https://en.wikipedia.org/wiki/Tejon_Ranch">Tejon Ranch</a> with friends from the UCLA Birding Club and the <a href="https://www.botgard.ucla.edu/whats_in_bloom/">UCLA Botanical Garden</a>. I had heard to Tejon as the largest privately held property within California and knew that the landscape was relatively undeveloped. I also knew that the managers of the property plan to develop a new &gt;20,000 unit housing development – a  plan that has been much criticized by a <a href="https://www.cnps.org/conservation/california-native-plant-society-condemns-la-county-decision-to-approve-the-centennial-development-13987">variety</a> <a href="https://www.latimes.com/opinion/op-ed/la-oe-anderson-tejon-ranch-20190102-story.html">of</a> <a href="https://www.biologicaldiversity.org/campaigns/save_tejon_ranch/">environmental</a> <a href="https://www.scpr.org/programs/airtalk/2018/12/04/63950/given-threat-of-wildfires-should-the-tejon-ranch-p/">groups</a>. There was lots more about the political back-and-forth that I did not know about – like the ranch managers <a href="https://www.latimes.com/opinion/op-ed/la-oe-anderson-tejon-ranch-20190102-story.html">banning one of the most knowledgeable botanists of the region</a> – but I also didn’t know just how incredible this landscape is. I will share some of the beauty of the landscape here.</p>

<p>We began the trip by visiting grasslands towards the western portion of the reserve. This portion has sandstone-derived soils, and the grasslands here were home to lots of beautiful poppies, pincushions, California goldfields, lupines, etc.</p>

<p><img src="/assets/tejon/DSC_0781.JPG" alt="" /></p>

<!--more-->

<p>There’s also a still-functioning cement mining operation in this portion of the ranch:</p>

<p><img src="/assets/tejon/DSC_0779.JPG" alt="" /></p>

<p>We saw lots of cool plants in this part of the reserve, including these:</p>

<p><img src="/assets/tejon/DSC_0774.JPG" alt="grapesoda lupine" /></p>
<blockquote>
  <p>Grape soda lupin (<em>Lupinus excubitus</em>) – it really does smell like its namesake!</p>
</blockquote>

<p><img src="/assets/tejon/DSC_0783.JPG" alt="Lomatium" /></p>
<blockquote>
  <p>A species of <em>Lomatium</em> in the Apiaceae – the carrot, parsely, and celery family.</p>
</blockquote>

<p><img src="/assets/tejon/DSC_0767.JPG" alt="Hordeum" /></p>
<blockquote>
  <p>A European grass <em>Hordeum</em> that has become invasive in lots of California grasslands also makes its appearance.</p>
</blockquote>

<p>On our drive to this part of the reserve, we were also incredibly lucky to catch a glimpse of the California pronghorn:</p>

<p><img src="/assets/tejon/DSC_0754.JPG" alt="pronghorn" /></p>

<p>Here it is zoomed in:</p>

<p><img src="/assets/tejon/DSC_0754-crop.JPG" alt="pronghorn-zoom" /></p>

<p>I had no idea these animals were in the area (I assumed the name Antelope Valley was a historical legacy which sadly no longer applies), so I was extra-excited to see these.</p>

<h4 id="into-the-oak-woodland-canyons">Into the Oak-woodland canyons</h4>

<p>From here we drove up into a canyon (I forget whether it was the Little or the Big Sycamore Canyon), where was saw some beautiful oak woodlands lanscape. I was charmed by lots of great California native plants:</p>

<p><img src="/assets/tejon/DSC_0805.JPG" alt="" /></p>
<blockquote>
  <p>An <em>Astragalus</em> (milkvetch)</p>
</blockquote>

<p><img src="/assets/tejon/DSC_0825.JPG" alt="" /></p>
<blockquote>
  <p>A beautiful flower of <em>Clarkia cylindrica</em> (I <em>think</em>).</p>
</blockquote>

<p><img src="/assets/tejon/DSC_0829.JPG" alt="" /></p>
<blockquote>
  <p>A native Californian thistle (<em>Cirsium occidentale</em>). There are so many invasive, nasty thistles around, that finding a native one felt really nice.</p>
</blockquote>

<p>We also saw a gorgeous gopher snake in this part of the reserve, but I didn’t get a photo.</p>

<h4 id="the-joshua-tree-desert">The Joshua Tree desert</h4>

<p>We moved from the woodland to what I thought was the most astonishing part of the reserve: its Joshua Tree desert section. The transition from the California grassland to the desert biomes was really special, with roadsides filled with representatives of both floras.</p>

<p><img src="/assets/tejon/DSC_0852.JPG" alt="" /></p>

<p>In some parts, the desert floor was covered with that look from afar like a monoculture of the desert dandilion <em>Malcothrix</em>.</p>

<p><img src="/assets/tejon/DSC_0863.JPG" alt="" /></p>

<p>And in other parts, the desert floor was blanketed by the California primrose:</p>

<p><img src="/assets/tejon/DSC_0882.JPG" alt="" /></p>

<p>For lunch we drove through this Joshua Tree desert into another canyon region, this one dominated by oaks, junipers, and pines. The transitions between these ecosystems was fantastic – again, it felt strange to see these distinct floras come together.</p>

<p><img src="/assets/tejon/DSC_0870.JPG" alt="" />
<img src="/assets/tejon/DSC_0888.JPG" alt="" /></p>

<p>There was also a lot of “charismatic microflora” in the desert:</p>

<p><img src="/assets/tejon/DSC_0899.JPG" alt="" /></p>
<blockquote>
  <p>The Padre’s blazing star, <em>Mentzelia pectinata</em></p>
</blockquote>

<p><img src="/assets/tejon/DSC_0897.JPG" alt="" /></p>
<blockquote>
  <p>The tiny but beautiful Bird’s eye <em>Gilia</em></p>
</blockquote>

<h4 id="up-the-mountains">Up the mountains</h4>

<p>We then drove up, up, and up – probably to &gt;7,000 feet – to checkout another distinct flora. This part of the reserve was characterized by a very “continental” flora – one that resembles the flora of the Utah and Nevada mountains.</p>

<p><img src="/assets/tejon/DSC_0905.JPG" alt="" /></p>

<p>There were lots of tiny annuals here too - like this <em>Microsteris</em>. I was amazed by how diverse the Polimoniaceae family was in this reserve!</p>

<p><img src="/assets/tejon/DSC_0906.JPG" alt="" /></p>

<p>Maddi made friends with a trailside grasshopper (well, she tried to, anyway – apparently it was quite bitey!)</p>

<p><img src="/assets/tejon/DSC_0908.JPG" alt="" /></p>

<p>There were lots of fallen logs with moss and lichen and fungi growing all over the bark, which made for some artsy shots:
<img src="/assets/tejon/DSC_0918.JPG" alt="" /></p>

<p>We drove a bit further along the trail, staying at the same elevation. The Continental flora that characterized our first stop gave way to a more California oak woodland/grassland flora as we drove west.</p>

<p><img src="/assets/tejon/DSC_0924.JPG" alt="" /></p>

<p>We were greeted by large fields of Baby Blue Eyes, which we had been seeing throughout the reserve in smaller numbers until now.</p>

<p><img src="/assets/tejon/DSC_0923.JPG" alt="" /></p>

<p>We also some a lot of redmaids – a plant that I have come to really like after first seeing it in Sedgwick a few years ago.</p>

<p><img src="/assets/tejon/DSC_0928.JPG" alt="" /></p>

<p>All of these vibrant colors resulted in some really pretty compositions…</p>

<p><img src="/assets/tejon/DSC_0932.JPG" alt="" /></p>

<h4 id="back-down-to-the-desert">Back down to the desert</h4>

<p>After spending some more time wandering the hills and looking out for condors (we didn’t find any – but we did see a pair of Golden Eagles, which was a treat!), we eventually drove back down to the desert at the base of the reserve. We stopped to take in the Joshua trees, and I finally got to get a closer look at their fruit, which was really cool:</p>

<p><img src="/assets/tejon/DSC_0961.JPG" alt="" /></p>

<p>I also got a better look at the desert dandilion <em>Malcothrix</em> - such a gorgeous plant!</p>

<p><img src="/assets/tejon/DSC_0960.JPG" alt="" /></p>

<p>And I also got to see some of my favorite species from my work at Sedgwick- like this Salivia (chia) plant, which is one of the species in my ongoing plant-microbe experiment in the greenhouse.</p>

<p><img src="/assets/tejon/DSC_0978.JPG" alt="" /></p>

<p>Driving back out of the reserve, it was great to reflect on the wide variety of landscapes we got to explore in a single day in Tejon reserve…</p>

<p><img src="/assets/tejon/DSC_0755.JPG" alt="" /></p>
:ET