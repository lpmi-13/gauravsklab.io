---
layout: post
title: Haruki Murakami's What I Talk About When I Talk About Running
categories: [bookreport]
---

I read this book after I started seriously preparing for the 2020 LA Marathon. I picked it up for two reasons. The first is that I wanted to continue cultivating my reading habit (I've read more than this list indicates, I promise). The second reason is that I wanted to get some insight into why other people run. 

Murakami's book gave me a lot of insight into why he runs, and I think his philosophy is a more thought-out version of many other runners' philosophy. While reading this book, I began to view running as an opportunity embrace solitude, an opportunity to practice persistence. No doubt I had entertained similar thoughts before picking up Murakami's memoir, but the book helped me transform my vague, passing thoughts into concrete realizations. 

<!--more-->



Murakami also expresses his own dislike of competing with others; instead, he seems to be always competing with an idealized (well, idealized within reason) version of himself. The beatiful thing is that Murakami rarely seems to live up to his own high standards. I have been feeling the same way of late. I am constantly falling short of certain internal goals. But as long as I enjoy the journey, I am learning to accept -- or even embrace -- these failures. I mean, if I fail to finish the LA marathon in under 4 hours, well, at least I'll have finished a marathon. And if I don't finish, then at least I will have tried, and done some really long runs (>15 miles!) to prepare. 