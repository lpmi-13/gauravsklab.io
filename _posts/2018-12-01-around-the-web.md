---
layout: post
title: Around the web (week of 26 November)
---

#### Reading
- [Mathematics as thought](https://aeon.co/essays/the-secret-intellectual-history-of-mathematics), by Mordechai Levy-Eichel for Aeon

- [On Being the Right Size](https://archive.org/stream/OnBeingTheRightSize-J.B.S.Haldane/rightsize_djvu.txt), by J.B.S. Haldane

> An angel whose muscles developed no more power weight for weight than those of an eagle or a pigeon would require a breast projecting forabout four feet to house the muscles engaged in working its wings, while to economize in weight, its legs would have to be reduced to mere stilts. Actually a large bird such as an eagle or kite does keep in the air mainly by moving its wings. It is generally to be seen soaring, that is to say balanced on a rising column of air.

- [Computational Ecology: From the Complex to the Simple and Back](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.0010018), by Mercedes Pascual in PLOS Computational Biology

#### Listening

- [Martin Rees' Reith Lectures](https://www.bbc.co.uk/programmes/b00sj9lh) from BBC   
- [70s Japanese Jazz Mix](https://www.youtube.com/watch?v=s-jtdKjzQaE) on Youtube   
- [Tech N9ne and Krizz Kaliko](https://www.npr.org/2018/08/28/642631937/tech-n9ne-feat-krizz-kaliko-tiny-desk-concert) on NPR Tiny Desk  