---
layout: post
title: Responding to the deluge
---

there is a war going on in america

its victims are women americans 
and african americans 
and latino americans 
and native americans 
and muslim americans 
and queer americans
and immigrant americans 

its perpetrators are white man americans

when they dont hurt us directly
they tell us to move on from the hurt we feel
they make us feel bad for caring
for wishing we could help 

but in a war we can fight back
at first we can fight back with kindness
at first we can limit how far we go
    out of our way

but in a war there comes a time, a place 
when the totality of our attention
ought to be trained on a single goal
the goal of preserving the people we love
the goal of preserving the people we have never met
    but we wish we did

with every injustice
every white supremacy inspired mass shooting
every attack on womens rights
every insult of african american heritage
every threat to latino american communities
every landgrab from native americans
every insult thrown at muslims
every indication that immigrants are not welcome

and 

every expression of the fact that white men neither feel 
   nor fear nor appreciate the devastation these cause

i inch closer 
and closer to 
embracing the 
fight back. 