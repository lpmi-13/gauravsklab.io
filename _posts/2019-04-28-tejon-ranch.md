---
layout: post
title: Tejon Ranch
---

I had the incredible opportunity to explore [Tejon Ranch](https://en.wikipedia.org/wiki/Tejon_Ranch) with friends from the UCLA Birding Club and the [UCLA Botanical Garden](https://www.botgard.ucla.edu/whats_in_bloom/). I had heard to Tejon as the largest privately held property within California and knew that the landscape was relatively undeveloped. I also knew that the managers of the property plan to develop a new >20,000 unit housing development -- a  plan that has been much criticized by a [variety](https://www.cnps.org/conservation/california-native-plant-society-condemns-la-county-decision-to-approve-the-centennial-development-13987) [of](https://www.latimes.com/opinion/op-ed/la-oe-anderson-tejon-ranch-20190102-story.html) [environmental](https://www.biologicaldiversity.org/campaigns/save_tejon_ranch/) [groups](https://www.scpr.org/programs/airtalk/2018/12/04/63950/given-threat-of-wildfires-should-the-tejon-ranch-p/  ). There was lots more about the political back-and-forth that I did not know about -- like the ranch managers [banning one of the most knowledgeable botanists of the region](https://www.latimes.com/opinion/op-ed/la-oe-anderson-tejon-ranch-20190102-story.html) -- but I also didn't know just how incredible this landscape is. I will share some of the beauty of the landscape here.

We began the trip by visiting grasslands towards the western portion of the reserve. This portion has sandstone-derived soils, and the grasslands here were home to lots of beautiful poppies, pincushions, California goldfields, lupines, etc.

![](/assets/tejon/DSC_0781.JPG)

<!--more-->

There's also a still-functioning cement mining operation in this portion of the ranch:

![](/assets/tejon/DSC_0779.JPG)

We saw lots of cool plants in this part of the reserve, including these: 

![grapesoda lupine](/assets/tejon/DSC_0774.JPG)
> Grape soda lupin (*Lupinus excubitus*) -- it really does smell like its namesake!

![Lomatium](/assets/tejon/DSC_0783.JPG)
> A species of *Lomatium* in the Apiaceae -- the carrot, parsely, and celery family.

![Hordeum](/assets/tejon/DSC_0767.JPG)
> A European grass *Hordeum* that has become invasive in lots of California grasslands also makes its appearance. 

On our drive to this part of the reserve, we were also incredibly lucky to catch a glimpse of the California pronghorn:

![pronghorn](/assets/tejon/DSC_0754.JPG)

Here it is zoomed in: 

![pronghorn-zoom](/assets/tejon/DSC_0754-crop.JPG)

I had no idea these animals were in the area (I assumed the name Antelope Valley was a historical legacy which sadly no longer applies), so I was extra-excited to see these. 

#### Into the Oak-woodland canyons 

From here we drove up into a canyon (I forget whether it was the Little or the Big Sycamore Canyon), where was saw some beautiful oak woodlands lanscape. I was charmed by lots of great California native plants:

![](/assets/tejon/DSC_0805.JPG)
> An *Astragalus* (milkvetch) 

![](/assets/tejon/DSC_0825.JPG)
> A beautiful flower of *Clarkia cylindrica* (I *think*). 

![](/assets/tejon/DSC_0829.JPG)
> A native Californian thistle (*Cirsium occidentale*). There are so many invasive, nasty thistles around, that finding a native one felt really nice.

We also saw a gorgeous gopher snake in this part of the reserve, but I didn't get a photo.

#### The Joshua Tree desert

We moved from the woodland to what I thought was the most astonishing part of the reserve: its Joshua Tree desert section. The transition from the California grassland to the desert biomes was really special, with roadsides filled with representatives of both floras.

![](/assets/tejon/DSC_0852.JPG)

In some parts, the desert floor was covered with that look from afar like a monoculture of the desert dandilion *Malcothrix*.

![](/assets/tejon/DSC_0863.JPG)

And in other parts, the desert floor was blanketed by the California primrose:

![](/assets/tejon/DSC_0882.JPG)

For lunch we drove through this Joshua Tree desert into another canyon region, this one dominated by oaks, junipers, and pines. The transitions between these ecosystems was fantastic -- again, it felt strange to see these distinct floras come together.

![](/assets/tejon/DSC_0870.JPG)
![](/assets/tejon/DSC_0888.JPG)

There was also a lot of "charismatic microflora" in the desert: 

![](/assets/tejon/DSC_0899.JPG)
> The Padre's blazing star, *Mentzelia pectinata*

![](/assets/tejon/DSC_0897.JPG)
> The tiny but beautiful Bird's eye *Gilia*


#### Up the mountains

We then drove up, up, and up -- probably to >7,000 feet -- to checkout another distinct flora. This part of the reserve was characterized by a very "continental" flora -- one that resembles the flora of the Utah and Nevada mountains. 

![](/assets/tejon/DSC_0905.JPG)

There were lots of tiny annuals here too - like this *Microsteris*. I was amazed by how diverse the Polimoniaceae family was in this reserve!

![](/assets/tejon/DSC_0906.JPG)

Maddi made friends with a trailside grasshopper (well, she tried to, anyway -- apparently it was quite bitey!)

![](/assets/tejon/DSC_0908.JPG)

There were lots of fallen logs with moss and lichen and fungi growing all over the bark, which made for some artsy shots:
![](/assets/tejon/DSC_0918.JPG)

We drove a bit further along the trail, staying at the same elevation. The Continental flora that characterized our first stop gave way to a more California oak woodland/grassland flora as we drove west. 

![](/assets/tejon/DSC_0924.JPG)

We were greeted by large fields of Baby Blue Eyes, which we had been seeing throughout the reserve in smaller numbers until now. 

![](/assets/tejon/DSC_0923.JPG)

We also some a lot of redmaids -- a plant that I have come to really like after first seeing it in Sedgwick a few years ago. 

![](/assets/tejon/DSC_0928.JPG)

All of these vibrant colors resulted in some really pretty compositions...

![](/assets/tejon/DSC_0932.JPG)

#### Back down to the desert

After spending some more time wandering the hills and looking out for condors (we didn't find any -- but we did see a pair of Golden Eagles, which was a treat!), we eventually drove back down to the desert at the base of the reserve. We stopped to take in the Joshua trees, and I finally got to get a closer look at their fruit, which was really cool:

![](/assets/tejon/DSC_0961.JPG)

I also got a better look at the desert dandilion *Malcothrix* - such a gorgeous plant!

![](/assets/tejon/DSC_0960.JPG)

And I also got to see some of my favorite species from my work at Sedgwick- like this Salivia (chia) plant, which is one of the species in my ongoing plant-microbe experiment in the greenhouse.

![](/assets/tejon/DSC_0978.JPG)

Driving back out of the reserve, it was great to reflect on the wide variety of landscapes we got to explore in a single day in Tejon reserve...

![](/assets/tejon/DSC_0755.JPG)