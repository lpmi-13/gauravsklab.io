---
layout: post
title: Around the web (week of 10 March)
---

#### Reading

- [The history of HBCUs in America](http://www.americanradioworks.org/segments/hbcu-history/), by Samara Freemark on American RadioWorks. I felt it important to learn more about the history of HBCUs in light of the godawful college admissions "scandal" this week

> Though black schools represent a tiny percentage of American colleges — around 3 percent of schools – they produce 24 percent of black STEM grads and confer almost 35 percent of all bachelor’s degrees earned by black graduates in astronomy, biology, chemistry, math, and physics. According to a report from the National Science Foundation, eight of the top 10 institutions producing black undergrads who went on to earn science and engineering doctorates were HBCUs.

<!--more-->

- [When the monsoon goes away ](https://aeon.co/essays/the-life-and-possible-death-of-the-great-asian-monsoon) by Sunil Amrith on Aeon

> A monsoon’s mechanisms are so complex they resist modelling. We can send crafts to other planets but no one can predict how much rain the monsoon will bring. Accelerating climate change brings even more uncertainty. If a fundamental shift in the patterns of the monsoon comes, it will devastate the livelihoods of hundreds of millions of people.

> Far from being a ‘natural’ disaster, he argued in 2001, the millions who died in the famines of the 1870s, and again in the 1890s, were ‘murdered … by the theological application of the sacred principles of Smith, Bentham and Mill’. Yes, the rains failed on a colossal scale, but what turned drought into disaster was imperial policy. Over the long term, British rule undermined the resilience of rural communities as they dragged India into modern capitalism. Over the short term, British rule made things worse by denying relief to starving people in the name of the integrity of ‘free’ markets.

- [At Major Engineering Conferences, Women Are Still Hard to Find](https://undark.org/article/optical-fiber-communications-conference-women/), by Ramin Skibba for Undark Magazine

> Indeed, for general prizes, like the John Tyndall Award, “presented to an individual who has made outstanding contributions in any area of optical-fiber technology,” all recipients since it began in 1987 have been men.


### Listening

- [Slam Poet Steve Coleman](https://www.youtube.com/watch?v=eOum8NE5-Ns)'s "I wanna hear a poem"  
- [Kendrick Lamar's performance](https://www.youtube.com/watch?v=QeFwtA3p4Mw) at the 2018(?) Grammys
