---
layout: post
title: Around the web (week of 9 June)
---

#### Reading
- [The Simply Complex: Trendy buzzword or emerging new science?](https://www.santafe.edu/research/results/sfi-press/worlds-hidden-plain-sight), by John Casti, available in "Worlds Hidden in Plain Sight".

> A more direct reason for trying to create a science of the complex is to get a handle on the limits of reductionism as a universal problem-solving approach.


<!--more-->


- [A Twisted Path to Equation-Free Prediction](https://www.quantamagazine.org/chaos-theory-in-ecology-predicts-future-populations-20151013/), by Gabriel Popkin for Quanta

>  Empirical dynamic modeling, by contrast, seamlessly incorporates new data and is always improving. Takens’ theorem works best when there are enough data points to make a dense attractor, making it easier to find times when a system’s present state is close to a past one. Any new data points will help users to see where a system is going to go next. 


- [Many Anti-Vaxxers Don’t Trust Big Pharma. There’s a Reason for That.](https://undark.org/article/anti-vaxxers-vaccines-trust-big-pharma/), by Teresa Carr for Undark Magazine

> There was "a sense that medicine was all about business and all about money," she says. "And that they were now consumers of health care rather than patients who had a doctor who cared for them -- cared about them as well as cared for them."



#### Listening

- [Masta Ace on NPR Tiny Desk](https://www.youtube.com/watch?v=3CxvB0d5VF0)  
- [Rang de Basanti](https://www.youtube.com/watch?v=c769V25pX08), from the movie of the same name  
- [When will Indian food be American?](http://www.sporkful.com/live-when-will-indian-food-be-american/), on Sporkful.  
