---
layout: post
title: Around the web (week of 17 March)
---

#### Reading

- [The Trouble With Biden](https://www.nytimes.com/2019/03/11/opinion/biden-busing-integration.html), by Jamelle Bouie for the NY Times. 

> Consider the message this would send. For decades Biden gave liberal cover to white backlash. He wasn’t an incidental opponent of busing; he was a leader who helped derail integration. He didn’t just vote for punitive legislation on crime and drugs; he wrote it. His political persona is still informed by that past, even if he were to repudiate those positions now. Biden could lead Democrats to victory over Trump, but his political style might affirm the assumptions behind Trumpism. The outward signs of our political dysfunction would be gone, but the disease would still remain.

<!--more-->

- [A World Built on Sand and Oil](https://www.laphamsquarterly.org/trade/world-built-sand-and-oil), by Laleh Khalili for Lapham's Quarterly, via [Longreads](https://longreads.com/). Another installment in my ongoing fascination/dismay at the global demand for sand.

> Countries with long coastlines and rich riverine topographies have become prey to other states and their own profit-seeking businessmen ravening for sand. Legal and illegal miners have stripped the rivers of Myanmar and Cambodia of their sandy riverbeds and sandbanks, dramatically changing flow patterns in rivers. The modified quality and volume of the sediments in such rivers make previously bountiful ecosystems inhospitable to agriculture and fishing

- [Speeding Cannabis Resentencing with Digital Technology](https://lasentinel.net/speeding-cannabis-resentencing-with-digital-technology.html) at the LA Sentinel.

> When voters approved Proposition 64 to legalize adult use of marijuana in November 2016, they also cleared the way for people convicted of possession, cultivation and sales or transport of cannabis to have their sentences reduced or their convictions dismissed.  
Fewer than 1,000 people have filed a petition for reclassification or resentencing, out of an estimated 60,000-100,000 eligible individuals, according to Ridley-Thomas.

### Listening 
I went to an [incredible concert](https://theatre.acehotel.com/events/cap-ucla-presents-roberto-fonseca-fatoumata-diawara/) yesterday at the Ace Theater in Downtown LA, so this week's listening is of the two performers from last night: 

- [Roberto Fonseca](https://www.youtube.com/watch?v=7t7WoIGEKpE)'s stunning jazz piano work  
- [Fatoumata Diawara](https://www.youtube.com/watch?v=IltMObtR7S4)'s incredibly energetic and lifting performances.  