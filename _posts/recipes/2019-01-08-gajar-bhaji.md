---
title: Gajar (carrot) bhaji
categories: [recipes-bhaji]
layout: post
hidden: true
---

#### Ingredients
1. Shredded carrots.

#### Steps
1. Make a phodni with jeera, turmeric, red chili pepper, hing.
2. Add shredded carrots, sautee for 2 minutes
3. Add salt to taste and a small pinch of sugar, put on a lid for 2 minutes
4. Again, sautee for 2 minutes.
5. Salt again if needed.
6. Add peanut chutney if available (or even just ground, roasted peanuts. )
