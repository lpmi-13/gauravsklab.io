---
title: Green beans bhaji
categories: [recipes-bhaji]
layout: post
hidden: true
---

#### Ingredients
1. 1 pound of fresh green beans
2. Phodni  
3. Brown sugar

#### Steps
1. Wash and destring the green beans, and cut into ~.5-1cm pieces.  
2. Make a phodni of mustard seeds, hinga, jeera seeds in a pressure cooker.  
3. Toss in the cut green beans  
4. Add turmeric, red chilli, salt, and masala. **Also add a spoonful of brown sugar**  
5. Add a little (~0.25 cups, or even less) water.  
6. Shut the lid, and turn off after 1 whistle. 
