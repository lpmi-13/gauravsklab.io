---
title: Peanut chutney
categories: [recipes-basics]
layout: post
hidden: true
---

This is a basic thing to have around the house:

Blend together cumin powder, a few fresh cloves of garlic (4 cloves/cup of peanut), roasted peanuts, sugar, salt, and some chili powder.
