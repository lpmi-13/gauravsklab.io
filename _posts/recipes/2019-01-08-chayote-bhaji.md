---
title: Chayote squash bhaji
categories: [recipes-bhaji]
layout: post
hidden: true
---

*Note*: can substitute kohlrabi for  chayote squash

#### Main ingredients
1. 1-2 Chayote squash (sub. kohlrabi).

#### Steps
1. If desired, remove skin from squash and cube into 1cm^3 pieces
2. Soak a handful of chana dal in water and microwave for 2 minutes.
3. Made phodni with mustard and jeera. Turn off heat and add hing, turmeric, and chili powder.
4. Toss in the cubed squash and sautee the 1-2 minutes.
5. Toss in a tomato or two, depending on availability (1 tomato per squash minimum)
6. Toss in the microwaved chana dal.
7. Add salt to taste (liquid should be a bit over-salty) and a pinch of sugar.
8. Add ~1/4 cup water-- not too much.
9. Pressure cook on medium heat for two whistles.
