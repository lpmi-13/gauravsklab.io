---
title: Basic phodni
categories: [recipes-basics]
layout: post
hidden: true
---

This is ultra-basic; almost all recipes can use some version of the following phodni:

1. Heat oil (any type is fine; for cooking, peanut or vegetable oil is good).
2. Once the oil is hot, add in cumin and/or mustard seeds (there are exceptional cases where only one of the two are ideal)
3. Move the phodni off of heat, and add turmeric, hing, red chilli powder (or dried red chilli, or both).
4. Some dishes will call for garlic to be tossed into the phodni; in most cases, though, we can use ginger+garlic paste (often added with onion).
