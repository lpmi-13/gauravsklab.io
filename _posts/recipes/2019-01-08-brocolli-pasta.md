---
title: Brocolli cumin pasta
categories: [recipes-otherMeals]
layout: post
hidden: true
---

#### Ingredients
1. Pasta (penne-type thing works best)
2. Brocolli
3. Parmesan (if avaiable; other cheeses used judiciously would be fine)
4. Ricotta (if available)

#### Steps
1. Toss bite-sized pieces of brocolli with olive oil and lots of cumin and black pepper, and salt, and red pepper flakes.
2. Bake at 425 degrees and roast for about 25-30 minutes
3. Boil pasta and drain.
4. Stir together parmesan (or whatever cheese) with some lime juice and salt in a bowl
5. Toss pasta and the cheese in with the roasted veggies.
6. Add in ricotta if using. 
