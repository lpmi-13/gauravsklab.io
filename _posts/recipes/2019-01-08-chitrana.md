---
title: Chitranna (lemon rice)
categories: [recipes-rice]
layout: post
hidden: true
---

#### Ingredients
1. Cooked rice (day or a few hours old)
2. Green chilli (maybe thai chilli)-- 3 or 4 

#### Steps
1. Make a phodni of cumin, mustard seeds, dried red chilli
2. Throw in the fresh chillis and fry a bit
3. Fry peanuts and cashews in the phodni
4. Turn to low heat and add turmeric.
5. Add all of the phodni to the cooked rice.
6. Also add juice from 3 limes and salt. Mix thoroughly. It should taste a bit over-tangy; evidently the rice will continue to absorb the acid over time before you eat.
7. Serve with yogurt.
