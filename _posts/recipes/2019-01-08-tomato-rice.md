---
title: Tomato rice
categories: [recipes-rice, basics, rice]
layout: post
hidden: true
---

*Notes*: Use big pressure cooker for this recipe. Makes enough for 4-5 individual meals. 

#### Ingredients
1. A can of tomato
2. Basmati rice (2 cups flat)
3. Fresh green chillis (1-2)
4. Grated ginger and garlic
5. Cashews or peanuts
6. Peas 
7. Optional: Onion, carrots, a diced potato.
8. Garam masala (if you prefer, can add a small cinnamon stick, 2 cardamom pods, 4 cloves, 6-8 peppercorns, 1 bay leaf)

#### Steps

0. Wash the rice and keep soaking aside in 2.5 cups of water. 
1. Grind the can of tomato and chilli together with some (1/2 tsp-ish) salt.   
2. Make standard phodni (mustard seeds, cumin seeds, dried red chilli, turmeric)  
3. Add onion, ginger, and garlic.
4. Add peas, carrots and potato (if using). Sautee for a minute with some salt (1/2 tsp-ish).
5. Add cashews or peanuts.  Sautee for another minute (note that cashews burn easily so keep an eye on them!)  
6. Add the ground tomato from Step 1. Sautee for 2-3 minutes. 7. Add soaked rice with all of the water. Stir thoroughly; taste water and add salt if needed (the water should be a little more salty than you want your food to be). 
8. Pressure cook for 1 whistle.  
