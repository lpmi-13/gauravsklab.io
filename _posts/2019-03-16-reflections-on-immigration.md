---
layout: post
title: Some reflections on Islamaphobia, anti-immigrant tendencies, and violence
---

I am writing this the day after the horrible mass shootings in New Zealand in which so many innocent, decent people lost their lives. I attended a vigil organized by the UCLA Muslim Student Association, where as part of a guided discussion we were asked to share our feelings in response to this news. I am writing this post because I am still thinking through my feelings, and am hoping that putting words on paper will help me organize my currently scattered thoughts. 

<!--more-->

The comment from the vigil that keeps reverberating in my mind is one that was made in response to the question of whether people feel safe on campus or in our LA community. A young muslim student, who wears a hijab, had been quiet for a long time, but spoke up to very powerfully explain that as a "visibly Muslim woman", she can *never* feel safe living her life in the US. That she "always feel like a target". That she can never take any day for granted, and that she has developed a fear of open spaces. 

The author of the following Twitter thread shares some similarly powerful sentiments (thanks to [Maddi](https://twitter.com/maddicowen) for sharing this thread):

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">While you&#39;re still horrified by the mosque shooting, I&#39;m going to share something I don&#39;t normally share.  Because tomorrow, it won&#39;t be so fresh, and that feeling you have right now, where even a small, kindhearted country like NZ isn&#39;t safe won&#39;t be filling your heart the 1/</p>&mdash; Vianna Goodwin (she/her) ☪️🏳️
🌈💜💖💙 (@GoodwinVianna) <a href="https://twitter.com/GoodwinVianna/status/1106412865408385024?ref_src=twsrc%5Etfw">March 15, 2019</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

--------------------

Another student at the vigil said that she feels "desensitized" to these horrible massacres, which have happened so frequently over the last few years. To be honest, I think that I have gone the other way. Until recently I never felt "sensitive" to these events- I always feel deeply sorry to those who lost their lives, and their loved ones whose lives will never be the same again, but I always figured that these events were so random, unpredictable, and logic-defying that I just couldn't be "sensitive" to them. I'm not proud of this reaction- but that's how it went.

But at the vigil, one of the leaders of the Muslim Student Associations mentioned a Hadith that is helping me interpret my responses:

> When any limb aches, the whole body aches. 

The muslim community is a limb of our community. So too are the LGBTQ community, the undocumented immigrant community, the African American community, the Native American community, the Sikh community, the *documented* immigrant community, and many more. These limbs have been aching for decades or centuries in the US and around the world, and their ache has intensified over the last few years with the (re)rise of White Nationalism. 

I think that with every new report of a massacre, I am becoming more and more sensitive to the pain to our larger community. I find that it is becoming harder by the day to suppress my anger towards the perpatrators of White Nationalism and empathy towards the victims. These feelings get in the way of my work, and make it harder for me to socialize without guit. Intellectually, I know that engaging with these feelings doesn't do anything to improve the world. But it's hard to ignore them, especially when I am trying to focus my mental energy on solving problems in my research (which doesn't do very much to help ease the pain in our community, either). 

I was advised once by a scientist I respect very much (who incedentally is a white American man) that I "should" be able to set my anger and empathy aside when I sit down to work so that these feelings don't distract me and make me a less productive scientist, but I find this advice more and more absurd by the day.

This post is quite rambling, and I don't know where it leaves me. But I am glad to have engaged with these gnawing thoughts.