---
layout: post
title: SatRday recap
---

Last weekend, I attended the excellent [SatRday conference](https://losangeles2019.satrdays.org/) here at the UCLA campus. I heard some excellent talks, and also presented a lightening talk. 

<!--more-->

I especially enjoyed the talks by Henrik Bengtsson on the [`future`](https://github.com/satRdays/losangeles/blob/master/2019/slides/05BengtssonH_20190406-SatRdayLA2019%2Cflat.pdf) package, which I can envision using, even for medium-sized computation. I also enjoyed Neal Fultz's talk on using [Declare Design](https://declaredesign.org/library/) for simulating experimental data to do more powerful experimental design work. I also really enjoyed the general atmosphere -- it was a great mix of academic and professional useRs, and everyone seemed really happy to  be there. Hats off to the organizers. 

I also gave a 5-minute lightening talk titled "[Using R for teaching ecology](https://github.com/satRdays/losangeles/blob/master/2019/slides/08LightningTalks/02gkandlikar-r-for-teaching-ecology.pdf)". At the beginning of the conference, I was starting to feel a lot of imposter syndrome -- here I was talking to a room of R experts about my modest attempts at using R in the classroom -- but the atmosphere I described above helped me overcome the imposter syndrome by about lunch. 

In this talk I made the case that using R and Shiny is a compelling way to make quantitative and computationally challenging concepts from ecology more inviting for undergraduate students, many of whom come into ecology classrooms thinking of themselves as "not math" or "not computer" students. (I would know; I was one such student seven years ago.) I shared my work on [ranacapa](https://gauravsk.shinyapps.io/ranacapa), the tool I built to help undergraduate students visualize results from eDNA studies, and also used my talk to encourage ecology instructors to use Shiny apps to teach things like [Lotka-Volterra competition](https://gauravsk.shinyapps.io/lotka)

![slide-from-talk](/assets/satRday-slide.png)