---
layout: post
title: Around the web (week of 25 March) (feat. photos from Antelope Valley)
---

#### Reading 

- [The climate movement needs more people like me](https://grist.org/article/the-climate-movement-needs-more-people-like-me/) by Irsa Hirsi on The Grist  

> When I joined my high school’s environmental club, I was the only black kid at most meetings, and usually the only person of color. The club consisted of a group of white students talking about their camping trips. I knew that I would never fit in. It was difficult to look like their token and attend their meetings, but I kept going because I wanted to change that.

<!--more-->

- [Preparing for Invasive Pests Before They Arrive](https://undark.org/article/preparing-for-invasive-pests-before-they-arrive/) by Stephanie Parker on Undark Magazine 

> National borders are human creations, of course, and insects and fungal diseases don’t abide by them. So even if an entire country could eradicate the fall armyworm, it would probably invade from a neighboring country all over again. 

#### Listening  

- [Rahul Sharma on the Santoor](https://www.youtube.com/watch?v=JTmACn6dkNE). I heard Rahul Sharma in concert with Zakir Hussain a while back at Royce Hall, and my love for both is strong as ever.  

#### Eye Candy

I had a great trip to the [Antelope Valley Poppy Reserve](http://www.parks.ca.gov/?page_id=627) this weekend with Marcel, Alex, and Annie. Some photos from the trip...


![creamcups](/assets/cream.JPG)
*nature is strikingly patchy in space*

![bee](/assets/bee.JPG)
*digger bee digging for pollen*

![goldfield](/assets/gold.JPG)
*golden fields of goldfields*
  