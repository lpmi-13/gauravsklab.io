---
layout: post
title: Around the web (week of 2 December)
---

#### Reading
- [How Academics Survive the Writing Grind: Some Anecdotal Advice](https://lithub.com/how-academics-survive-the-writing-grind-some-anecdotal-advice/), by Helen Sword for Lit Hub. I thoroughly enjoyed Sword's book on the same topic, [Air and Light and Space and Time](http://www.hup.harvard.edu/catalog.php?isbn=9780674737709)
- *For the biology nerds*- [Our recent pre-print](https://www.biorxiv.org/content/early/2018/12/07/488627) about Anacapa, a bioinformatics pipeline for assigning taxonomy to sequences in environmental DNA projects
- [What is Complexity?](http://www.bristol.ac.uk/bccs/about/complexity/) - a summary by the Bristol Center for Complexity Science. I paired this with [this introductory video](https://www.youtube.com/watch?v=Eo5oQ9Psmg8) from the Santa Fe Institute's Complexity Explorer.
- [Animals are riding an escalator to extinction](https://www.theatlantic.com/science/archive/2018/10/mountain-animals-are-riding-escalator-extinction/574294/), by Ed Yong for The Atlantic. Based on [this paper](https://www.pnas.org/content/115/47/11982).
- [What is a University?](https://www.kcet.org/arts-entertainment/10-questions-what-is-a-university)-  Reading associated with UCLA Arts ["10 Questions"](https://arts.ucla.edu/single/10-questions/) series.

#### Listening

- [Interview with Harsha Bhogle]() about the past, present, and future of (Indian) cricket. Fantastic listening, I suspect especially so for fellow Indian expats.
- [The Death and Life of the Great Lakes](https://www.wbur.org/onpoint/2018/11/09/death-and-life-great-lakes-dan-egan), from On Point
- [Los Hacheros](https://www.npr.org/2016/06/30/484232322/los-hacheros-tiny-desk-concert) at the NPR Tiny Desk
- [Anoushka Shankar's performance of Land of Gold at Glastonbury](https://www.youtube.com/watch?v=sNJJRxUvNKw). This music lifts me. 
