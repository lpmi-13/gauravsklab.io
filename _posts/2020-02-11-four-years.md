---
layout: post
title: The meaning of four years
---

Yesterday marked four years since my father's passing. Anniversaries have always woken in me a confusing, unsettling mix of emotions, and this one especially so. 

<!--more-->

It's strange to think that it has been four years since I heard dad ask questions inspired by his insatiable curiosity, or heard him reel off a series of specific facts from varied disciplines to support his point of view, or heard his wonderful laugh, heard him call me strange names like "Saleem Hyderabadi" (inexplicably inspired by my floral corduroy shirt) or "Na chin ta" (in reference to my small head). 

Some times, four year sounds like a lot, and some times, it sounds too little. 

This anniversary feels strange. Four years is the amount of time I was in high school, more than the amount of time I was in college, more than the age difference between me and Gautam, more than the amount of time that we've had to deal with the rot of the Trump administration. It is *four times* more than the amount of time I spent as an undergrad in George Weiblen's lab. It is more than a seventh of my life. 

So much must have happened in the past four years, but right now, at this moment, it feels like so little has happened. Why does it feel that way? My guess is that it feels this way because the milestones I've passed in the past four years feel pretty insignificant next to dad's passing. A metaphor from Agatha Christie's *Murder on the Nile* comes to mind. (Dad would be proud that I'm integrating Poirot into this piece). Several characters in that novel independently remark that "After the Sun rises, nobody notices the moon." I think it's like that with me. Against the backdrop of dad's passing, other landmarks fade away. 

Though I remain confused, I will take this opportunity to pay attention to the moons. In the past four years, mom has shined in so many ways. She has been resilient, persistent, committed. She is becoming an Elder Statesperson of our large extended family. Gautam too has grown. He has taken risks, he has brought people up with him, he has learned to express his vulnerabilities. He completed his Thirty Under Thirty. And of course, I have grown. I've gone on runs longer than I ever imagined I could (Gaurav Kandlikar, aspiring marathon runner -- really???), cooked interesting meals, developed a really wonderful relationship with Maddi, contributed to the scientific community. 

The sun will continue to burn bright throughout my life. I don't ever want that to change. It will be inevitable that meaningful moments in my life will feel underwhelming. But I now have some clarity to understand why that might be. And I hope this clarity will encourage me to periodically take stock of the moons around me. 