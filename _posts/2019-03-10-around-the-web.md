---
layout: post
title: Around the web (week of 3 March)
---

#### Reading

- [Lost World](https://emergencemagazine.org/story/lost-world/) by Kalyanee Mam for Emergence Magazine. 
> As Singapore dredges sand out from beneath Cambodia’s mangrove forests, an ecosystem, a communal way of life, and one woman’s relationship to her beloved home are faced with the threat of erasure.

- [The Impossible Mathematics of the Real World](http://nautil.us/issue/69/patterns/the-impossible-mathematics-of-the-real-world-rp) by Evelyn Lamb for Nautilus 
> The story goes that in 1978 mathematician John McKay made an observation both completely trivial and oddly specific: 196,884 = 196,883 + 1. The first number, 196,884, had come up as a coefficient in an important polynomial called the j-invariant, and 196,883 came up in relation to an enormous mathematical object called the Monster group. Many people probably would have shrugged and moved along, but the observations intrigued some mathematicians, who decided to take a closer look. They uncovered connections between two seemingly unrelated subjects: number theory and the symmetries of the Monster group. 

- [Gasping for Air in India’s Industrial North](https://undark.org/article/air-pollution-patna/) by Gayathri Vaidyanathan for Undark. 
> In the winter [in Patna], cold air and pollution arising from a mix of wood-burning cook stoves, smoldering garbage, brick kilns, construction, and road dust get trapped close to the ground. Emissions accrue into a smothering blanket that spreads out across the region

#### Listening 

- [Avishai Cohen Trio](https://www.youtube.com/watch?v=K6s7Et1X0C4) at Jazz à la Villette