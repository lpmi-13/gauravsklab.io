---
layout: post
title: Around the web (week of 9 December)
---

#### Reading

- [Co-occurrence of linguistic and biological diversity in
biodiversity hotspots and high biodiversity
wilderness areas](https://www.cbd.int/financial/doc/PNAS-2012-Gorenflo-8032-7.pdf), by Gorenflo et al. in PNAS (2012)
> Languages in high biodiversity regions also often co-occur with one or more specific conservation priorities, here defined as endangered species and protected areas, marking particular localities important for maintaining both forms of diversity  

- [Who Was Ramanujan?](https://blog.stephenwolfram.com/2016/04/who-was-ramanujan/), by Stephen Wolfram for his blog.   

- [Can Dirt Save the Earth?](https://www.nytimes.com/2018/04/18/magazine/dirt-save-earth-carbon-farming-climate-change.html?src=longreads), by Moises Velasquez-Manoff for NY Times Magazine  

- [This ‘Equity’ picture is actually White Supremacy at work](https://medium.com/@eec/this-equity-picture-is-actually-white-supremacy-at-work-59f4ea700509) by Sippin the EquiTEA on Medium. A critique of the widely used "equity" diagram:
![](https://cdn-images-1.medium.com/max/1600/1*Sbu0UfWk6FZGoUIYFGqrUA.png)
#### Listening

- [Good as Hell](https://www.youtube.com/watch?v=SmbmeOgWsqE), by the incredible Lizzo. To belabor the goodness of this song, read *[In Praise Of 'Good As Hell,' The Song That Believes In You Even When You Don't](https://www.npr.org/2018/11/28/671236822/lizzo-good-as-hell-american-anthem?src=longreads)*  

- [Is there a Jewish way of talking?](https://player.fm/series/series-2355478/is-there-a-jewish-way-of-talking), an episode of the entertaining Slate podcast *Lexicon Valley*